build: report.tex
	latexmk -pdf -synctex=1 report.tex
	makeglossaries report
	rm report.aux
	latexmk -pdf -synctex=1 report.tex
	evince report.pdf

clean:
	latexmk -CA
	rm -f *.gls *.glo *.glg *.ist *.bbl *.tex~
